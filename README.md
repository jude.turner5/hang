# Hang

💞 Simple discord selfbot that's coded in python used to clear all messages! (default: .dd)
----------

Contact
----------
* [OGU](https://ogusers.com/06)
* [Telegram](tg://resolve?domain=triston)
     
Steps
----------
* install [python 3](https://www.python.org/ftp/python/3.9.1/python-3.9.1-amd64.exe)
* open a shell (cmd prompt or powershell doesn't matter)
  # Linux / macOS
    python3 -m pip install -U discord.py

  # Windows
    py -3 -m pip install -U discord.py

* configure config.json (token, prefix, command)
* run Start Selfbot.bat

Credits
----------
 * [micah (og creator)](http://github.com/girl)
 * [jack](http://gitlab.com/woman)

